var test__dct__perform_8h =
[
    [ "INIX", "dd/da1/test__dct__perform_8h.html#ac1ab3dc06b6c2f1e45cb83fb58b5bc79", null ],
    [ "ENDX", "dd/da1/test__dct__perform_8h.html#ad55588a3f3c4ab37a0098072f6414a12", null ],
    [ "INIY", "dd/da1/test__dct__perform_8h.html#ae147f6613ed3c602dad485805f1a0939", null ],
    [ "ENDY", "dd/da1/test__dct__perform_8h.html#aa8a1b4147c25510d931fdb91ca9e3c5a", null ],
    [ "LEFT", "dd/da1/test__dct__perform_8h.html#a437ef08681e7210d6678427030446a54", null ],
    [ "RIGHT", "dd/da1/test__dct__perform_8h.html#a80fb826a684cf3f0d306b22aa100ddac", null ],
    [ "UP", "dd/da1/test__dct__perform_8h.html#a1965eaca47dbf3f87acdafc2208f04eb", null ],
    [ "DOWN", "dd/da1/test__dct__perform_8h.html#a4193cd1c8c2e6ebd0e056fa2364a663f", null ],
    [ "NGBRS", "dd/da1/test__dct__perform_8h.html#ae44f6e3e6bf17dd64989c4fbdcdea649", null ],
    [ "HORTAG", "dd/da1/test__dct__perform_8h.html#a4e39d3cac3cc09bb2948cb608b0709de", null ],
    [ "VERTAG", "dd/da1/test__dct__perform_8h.html#ae9a1291aa4d1ca50a92a782346a70501", null ],
    [ "dmax", "dd/da1/test__dct__perform_8h.html#a2b6f0c74c90c40c64f055c7490b7d185", null ],
    [ "update", "dd/da1/test__dct__perform_8h.html#ae3440d13a6e707eac1b65e1f079d4a3f", null ],
    [ "inidat", "dd/da1/test__dct__perform_8h.html#a9ef09b8e420a2e60e8b53d55f8990aee", null ]
];