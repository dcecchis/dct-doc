var dct__4d__var_8c =
[
    [ "DCT_Create_4d_Var", "d8/de0/dct__4d__var_8c.html#aa040b8ebf1a0a7433b7505419d853fa9", null ],
    [ "DCT_Set_4d_Var_Dims", "d8/de0/dct__4d__var_8c.html#a2a62c9139c640b73c2a8c0889fdb9ffc", null ],
    [ "DCT_Set_4d_Var_Val_Location", "d8/de0/dct__4d__var_8c.html#a3d683fb8199ee97f5ab1f67827244e2d", null ],
    [ "DCT_Set_4d_Var_Strides", "d8/de0/dct__4d__var_8c.html#abb7cbc28277d8a7d259cd3a6d7eae5f2", null ],
    [ "DCT_Set_4d_Var_Time", "d8/de0/dct__4d__var_8c.html#a3835a7fdc151dbfc943b64ba553b16d4", null ],
    [ "DCT_Set_4d_Var_Dist", "d8/de0/dct__4d__var_8c.html#a7b3cf6be03d4659c6a27f210bc3c4c9a", null ],
    [ "DCT_Set_4d_Var_Mask", "d8/de0/dct__4d__var_8c.html#a4f22cd5853b62751dee76ea2fc3e73e3", null ],
    [ "DCT_Set_4d_Var_Freq_Consumption", "d8/de0/dct__4d__var_8c.html#ac2829a851b30326ed8deaea81845e82f", null ],
    [ "DCT_Set_4d_Var_Freq_Production", "d8/de0/dct__4d__var_8c.html#a977976614bbf3fbdc4cb91ba8cd49e7d", null ],
    [ "DCT_Set_4d_Var_Labels", "d8/de0/dct__4d__var_8c.html#aa1dfcdb8b7746c9c8388b58bbbdec68b", null ],
    [ "DCT_Set_4d_Var_Units", "d8/de0/dct__4d__var_8c.html#adb4501c27e0ec081a258a17c63516f36", null ],
    [ "DCT_Set_4d_Var_Values", "d8/de0/dct__4d__var_8c.html#a4d5f0b7a0e582d00dfd8171eb14d090d", null ],
    [ "DCT_Destroy_4d_Var", "d8/de0/dct__4d__var_8c.html#a2bfa2fdcaa266604111df71e90c42825", null ]
];