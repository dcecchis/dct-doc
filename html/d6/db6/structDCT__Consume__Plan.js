var structDCT__Consume__Plan =
[
    [ "VarMsgTag", "d6/db6/structDCT__Consume__Plan.html#aca15518a3975bd3ac6a262fb25c9348e", null ],
    [ "Nmsg", "d6/db6/structDCT__Consume__Plan.html#aba6fbf4a408106c87fefd62f23c19b7d", null ],
    [ "reqsts", "d6/db6/structDCT__Consume__Plan.html#a10e9cc60fa0267de0fbed74588fea707", null ],
    [ "DType", "d6/db6/structDCT__Consume__Plan.html#aa884baf901f109ab57bb7dee72ce6f89", null ],
    [ "VarTransf", "d6/db6/structDCT__Consume__Plan.html#a5e713d61bc73c2d071ca5787ea435870", null ],
    [ "VarChUnits", "d6/db6/structDCT__Consume__Plan.html#a9b07bf6f12066ecc2e736b1e4e141998", null ],
    [ "VarIniPos", "d6/db6/structDCT__Consume__Plan.html#a492cb48ef494aeecb433d2ded0ece89d", null ],
    [ "VarNpts", "d6/db6/structDCT__Consume__Plan.html#a032f6c6590b4c823d53e409cf8a4ea69", null ],
    [ "VarDomType", "d6/db6/structDCT__Consume__Plan.html#a30938594c06706d69bad0106ff579a4a", null ],
    [ "VarTempVal", "d6/db6/structDCT__Consume__Plan.html#a7d68ec7bc81e1a828d97dccc6621c39d", null ],
    [ "VarLabels", "d6/db6/structDCT__Consume__Plan.html#a1854e45db11b21c94d8b1806832437a8", null ],
    [ "VarInt", "d6/db6/structDCT__Consume__Plan.html#a635f1a721ee6495c1cec0873262e0741", null ],
    [ "VarMask", "d6/db6/structDCT__Consume__Plan.html#a01770c1f4e0ae961291bc4eabd2ca2a8", null ],
    [ "VarRcvdDType", "d6/db6/structDCT__Consume__Plan.html#a7c27bb06306a0b92c451fc621bb86ec0", null ],
    [ "VarRcvdNpts", "d6/db6/structDCT__Consume__Plan.html#ab50afe66677fc8690dedd0e21c63e418", null ],
    [ "VarRcvdVal", "d6/db6/structDCT__Consume__Plan.html#a02fc855067e20825e6a19b9bc04a3ef9", null ]
];