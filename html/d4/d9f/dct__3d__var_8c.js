var dct__3d__var_8c =
[
    [ "DCT_Create_3d_Var", "d4/d9f/dct__3d__var_8c.html#a037051ac4c366398d3c0471059189341", null ],
    [ "DCT_Set_3d_Var_Dims", "d4/d9f/dct__3d__var_8c.html#ab92ab3d19fde2766fe79a9fb59abb95f", null ],
    [ "DCT_Set_3d_Var_Val_Location", "d4/d9f/dct__3d__var_8c.html#aad596b1a785978b76b029e02b23b5b44", null ],
    [ "DCT_Set_3d_Var_Strides", "d4/d9f/dct__3d__var_8c.html#a2909d58f6acca82ef4af7ed17d4b877d", null ],
    [ "DCT_Set_3d_Var_Time", "d4/d9f/dct__3d__var_8c.html#a7f6a00c658a80f6f35602589d73de9d6", null ],
    [ "DCT_Set_3d_Var_Dist", "d4/d9f/dct__3d__var_8c.html#a4d5475bac6322c72d2266603c45ba73d", null ],
    [ "DCT_Set_3d_Var_Mask", "d4/d9f/dct__3d__var_8c.html#a30f9cf1c853e417fe4149b5fe70af382", null ],
    [ "DCT_Set_3d_Var_Freq_Consumption", "d4/d9f/dct__3d__var_8c.html#aa1b1fbbec2841839a778205a82cde400", null ],
    [ "DCT_Set_3d_Var_Freq_Production", "d4/d9f/dct__3d__var_8c.html#a007b8b8259e6357d8b1bdd1fd58b1ce8", null ],
    [ "DCT_Set_3d_Var_Labels", "d4/d9f/dct__3d__var_8c.html#a9d1bd2642326da4bbe2113206967f4b3", null ],
    [ "DCT_Set_3d_Var_Units", "d4/d9f/dct__3d__var_8c.html#ae3c32a97f9db9291d1656ccbbb17edc8", null ],
    [ "DCT_Set_3d_Var_Values", "d4/d9f/dct__3d__var_8c.html#a2563984a63e9b5825bd69c947d2d014e", null ],
    [ "DCT_Destroy_3d_Var", "d4/d9f/dct__3d__var_8c.html#a363f2221c1a68056dd76d36783e65e50", null ]
];