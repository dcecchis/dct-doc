var dct__comm__data_8c =
[
    [ "DCT_Send_Field", "d9/db0/dct__comm__data_8c.html#a1031c36893a0ba3a951d1da4caaf4042", null ],
    [ "DCT_Send_3d_Var", "d9/db0/dct__comm__data_8c.html#a5f5694272c043894c5dc80f99f01200f", null ],
    [ "DCT_Send_4d_Var", "d9/db0/dct__comm__data_8c.html#af03f5d557f93bd3651f1711890236be9", null ],
    [ "DCT_Recv_Field", "d9/db0/dct__comm__data_8c.html#a80cf73b4edccdb6e21c4e203f9d1c37b", null ],
    [ "DCT_Recv_3d_Var", "d9/db0/dct__comm__data_8c.html#ae8b687923ced739386283b246af66153", null ],
    [ "DCT_Recv_4d_Var", "d9/db0/dct__comm__data_8c.html#a4aff755a39f91fe7fe9513254c7ab810", null ]
];