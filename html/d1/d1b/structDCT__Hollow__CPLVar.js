var structDCT__Hollow__CPLVar =
[
    [ "VarName", "d1/d1b/structDCT__Hollow__CPLVar.html#abbc96b3ba4bf06dd455b01c9e6e2675e", null ],
    [ "VarModel", "d1/d1b/structDCT__Hollow__CPLVar.html#aad021e5fe99755d738b5302db6d61a40", null ],
    [ "VarTag", "d1/d1b/structDCT__Hollow__CPLVar.html#a1b899d55b7b66e77674d9e9c2be4d998", null ],
    [ "CoupleVarTypes", "d1/d1b/structDCT__Hollow__CPLVar.html#aab4c114674dd91e9ed99b0bb887a7c27", null ],
    [ "VarUnits", "d1/d1b/structDCT__Hollow__CPLVar.html#a0e83c894c588821efbd996717d67e22b", null ],
    [ "VarTimeUnits", "d1/d1b/structDCT__Hollow__CPLVar.html#a834a1252c697fce3fa8d39ffaa9ec47f", null ],
    [ "VarTimeIni", "d1/d1b/structDCT__Hollow__CPLVar.html#aca91190aa6fd504fdfd1c406f4167333", null ],
    [ "VarFrequency", "d1/d1b/structDCT__Hollow__CPLVar.html#a90079f95c1dad1179113547a1e9b3020", null ],
    [ "VarProduced", "d1/d1b/structDCT__Hollow__CPLVar.html#a7b70a1cc0bf9a888fdc6f63a0566040d", null ],
    [ "VarUserDataType", "d1/d1b/structDCT__Hollow__CPLVar.html#abd21621056cf6e239ae7f62c9f171bce", null ]
];