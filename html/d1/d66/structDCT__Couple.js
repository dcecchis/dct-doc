var structDCT__Couple =
[
    [ "CoupleName", "d1/d66/structDCT__Couple.html#adc715cd00c1e712fea310b6c7f8f4e5e", null ],
    [ "CoupleTag", "d1/d66/structDCT__Couple.html#a1424c1e4c196964c89fcbd96fd77e71a", null ],
    [ "CoupleDescription", "d1/d66/structDCT__Couple.html#af5107d3f28e8c6daea4564c6a972794f", null ],
    [ "CoupleModelA", "d1/d66/structDCT__Couple.html#a63b479633da0487a1f4727ba3e1de3b5", null ],
    [ "CoupleModelB", "d1/d66/structDCT__Couple.html#a9efdd065e4e6b9b188ab8dd41fad4bae", null ],
    [ "CoupleHModelB", "d1/d66/structDCT__Couple.html#a22333eadca2d565aa5ae253e927bc96b", null ],
    [ "CoupleTotNumVars", "d1/d66/structDCT__Couple.html#ad2de85648ce946d74cf345206e8be008", null ],
    [ "CoupleNumVars", "d1/d66/structDCT__Couple.html#ad11cc928598f9cd7e04c7f1a87ba4154", null ],
    [ "CoupleNumVars2", "d1/d66/structDCT__Couple.html#aea1e67e3ffdccf61d9f34cb454681f2a", null ],
    [ "CoupleFirstVarCPL", "d1/d66/structDCT__Couple.html#a4814b1ad373aba0b72ca0bd5edc777dd", null ],
    [ "CouplingTable", "d1/d66/structDCT__Couple.html#a99d1129146d3e71cb05efbb13420e264", null ],
    [ "CoupleRcv", "d1/d66/structDCT__Couple.html#a0d55c233e45ea7932e896cecc32c2723", null ],
    [ "CoupleSnd", "d1/d66/structDCT__Couple.html#a53189088574941b47d4e7fb67eb783b9", null ]
];