var NAVTREE =
[
  [ "\"Distributed Coupling Toolkit (DCT)\"", "index.html", [
    [ "README", "da/d34/md__Users_dcecchis_DCT_Project_DCT_README.html", null ],
    [ "Todo List", "dd/da0/todo.html", null ],
    [ "Deprecated List", "da/d58/deprecated.html", null ],
    [ "Data Types List", "annotated.html", [
      [ "Data Types List", "annotated.html", "annotated_dup" ],
      [ "Data Types", "classes.html", null ],
      [ "Data Fields", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Variables", "functions_vars.html", "functions_vars" ]
      ] ]
    ] ],
    [ "Files", null, [
      [ "File List", "files.html", "files" ],
      [ "Globals", "globals.html", [
        [ "All", "globals.html", "globals_dup" ],
        [ "Functions/Subroutines", "globals_func.html", "globals_func" ],
        [ "Variables", "globals_vars.html", null ],
        [ "Typedefs", "globals_type.html", null ],
        [ "Enumerations", "globals_enum.html", null ],
        [ "Enumerator", "globals_eval.html", null ],
        [ "Macros", "globals_defs.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"annotated.html",
"d6/ddd/structDCT__Intersect__SubDom.html",
"dc/d6c/dct__mpi__comm_8c.html#a0d1392f03f4eaf364241823fd2868995",
"df/d15/dct_8h.html#a86537b81f0d79a498d3a926e9abbcf65afd9d9dae9ef97eeba44c264eae88121d"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';