var structDCT__Model__Dom =
[
    [ "ModDomDim", "db/da4/structDCT__Model__Dom.html#a82090cdc0233fb0bea8da261d0489aa9", null ],
    [ "ModDomType", "db/da4/structDCT__Model__Dom.html#ac77ee813c74253738ae369835ebc742a", null ],
    [ "ModNumPts", "db/da4/structDCT__Model__Dom.html#afd1e534ef7d522b7b03cda1fbacfcf34", null ],
    [ "ModValues", "db/da4/structDCT__Model__Dom.html#aaa83d54a7afcb9d8c50165cbdae00271", null ],
    [ "ModSubDomParLayout", "db/da4/structDCT__Model__Dom.html#ae36064b4cd566a6755ddc7c7a9f5f7c4", null ],
    [ "ModSubDomLayoutDim", "db/da4/structDCT__Model__Dom.html#a2ac7f91ac0f06a2e666cebc863a152cb", null ],
    [ "ModSubDom", "db/da4/structDCT__Model__Dom.html#a85b793118f619ed1724b9a70b1d4129e", null ]
];