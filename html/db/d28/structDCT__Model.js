var structDCT__Model =
[
    [ "ModName", "db/d28/structDCT__Model.html#a593a6930cf284460e9fff725d2ae0d65", null ],
    [ "ModDescription", "db/d28/structDCT__Model.html#a4b9de185dc76e325e7b8f52e104b57ac", null ],
    [ "ModCommit", "db/d28/structDCT__Model.html#a21effac15aa92402f0c19cce165ef4ba", null ],
    [ "ModTag", "db/d28/structDCT__Model.html#aa8d929820cde971242bc046275e125ac", null ],
    [ "ModNumProcs", "db/d28/structDCT__Model.html#a4ab4f31ef23229a748830d88cb3c8b2b", null ],
    [ "ModTimeIni", "db/d28/structDCT__Model.html#abd0c11ea487324324f521b9071494f5f", null ],
    [ "ModTime", "db/d28/structDCT__Model.html#a70ec08868471fbd30a6d009fc7fd0210", null ],
    [ "ModCurrentTime", "db/d28/structDCT__Model.html#aa3ea108bbf5a301a58689e3811e99041", null ],
    [ "ModTimeUnits", "db/d28/structDCT__Model.html#a285797d00e9fce6f7f2b9936918eb249", null ],
    [ "ModDomain", "db/d28/structDCT__Model.html#a8878902aa3cfa6dfa4c1dcd8b4bbdea7", null ],
    [ "ModTotVarConsumed", "db/d28/structDCT__Model.html#abbd74ce5916701e620d584111345ab32", null ],
    [ "ModTotVarProduced", "db/d28/structDCT__Model.html#a027993ded82a7ddd246a713180b17cd9", null ],
    [ "ModConsumeVars", "db/d28/structDCT__Model.html#a2261691e356cf0ac545be1d33d9b96bf", null ],
    [ "ModProduceVars", "db/d28/structDCT__Model.html#a3f8c75a813ec0ba885b448d94bb694bb", null ],
    [ "ModLeadRank", "db/d28/structDCT__Model.html#a3c284ccbb275ab1223f5ce2ddf0bf461", null ],
    [ "ModGroupComm", "db/d28/structDCT__Model.html#a9baf6db49e95c44b29f6610c9fc750ec", null ]
];