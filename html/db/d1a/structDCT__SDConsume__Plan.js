var structDCT__SDConsume__Plan =
[
    [ "Recvr", "db/d1a/structDCT__SDConsume__Plan.html#a0f713cfdff4e9db5c46d10c1cc6a676b", null ],
    [ "IniVal", "db/d1a/structDCT__SDConsume__Plan.html#a51719a2aa5626b1216137b01a3b6e16f", null ],
    [ "EndVal", "db/d1a/structDCT__SDConsume__Plan.html#a71e1686bc3ae346f778c8d3065ec1d71", null ],
    [ "GlbInd", "db/d1a/structDCT__SDConsume__Plan.html#afa9bedafcc8ca8d0ef9828b8cfeacd59", null ],
    [ "Npts", "db/d1a/structDCT__SDConsume__Plan.html#a99278ea113e53f01325d72e427b45faa", null ],
    [ "SndrNpts", "db/d1a/structDCT__SDConsume__Plan.html#af17e26d295620194655a628158808a09", null ],
    [ "SndrNSD", "db/d1a/structDCT__SDConsume__Plan.html#acd8fa03d02eb05fde26665facaddc4f4", null ],
    [ "SndrSDom", "db/d1a/structDCT__SDConsume__Plan.html#a7bef5b55cdd6aa78e480c8e38f70504f", null ],
    [ "Nmsg", "db/d1a/structDCT__SDConsume__Plan.html#a78a47c3f578a15418a8779688b7a2974", null ],
    [ "SDSlab", "db/d1a/structDCT__SDConsume__Plan.html#a94a63fd4b9c0a73e14248f9e18853f26", null ]
];