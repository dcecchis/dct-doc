var searchData=
[
  ['sdcomm',['SDComm',['../db/d01/structDCT__Var__Node.html#ab73909d4f09e006115f90b8c1ae0e34c',1,'DCT_Var_Node']]],
  ['sdslab',['SDSlab',['../db/d1a/structDCT__SDConsume__Plan.html#a94a63fd4b9c0a73e14248f9e18853f26',1,'DCT_SDConsume_Plan']]],
  ['seconds',['SECONDS',['../df/d15/dct_8h.html#a0ab162e2324b1b3bddd2a2f640065a0ba70367ff8e866216e6a822a2c952abfc1',1,'dct.h']]],
  ['sendr',['Sendr',['../db/d56/structDCT__SD__Consume.html#a527d3a3eb72066f43095924e220e9ed8',1,'DCT_SD_Consume::Sendr()'],['../d2/d24/structDCT__SDProduce__Plan.html#a551d92f97e659bf6fcef4d0ad7a65d4f',1,'DCT_SDProduce_Plan::Sendr()']]],
  ['set_5fdistribution',['Set_Distribution',['../d4/dd3/dctsys_8h.html#af7efe98c91bd5fb20e689867956e4025',1,'Set_Distribution(const DCT_Distribution, const DCT_Integer *, DCT_Integer **, DCT_Integer *):&#160;dct_data_utils.c'],['../da/df2/dct__data__utils_8c.html#a4531e24a12edb3cb594e1f28a02a88f7',1,'Set_Distribution(const DCT_Distribution dist, const DCT_Integer *procs, DCT_Integer **varprocs, DCT_Integer *dimtotal):&#160;dct_data_utils.c']]],
  ['set_5fdistribution_5ftype',['Set_Distribution_Type',['../d4/dd3/dctsys_8h.html#a4ddf33527aad25d3d584f0d8e3f41a7d',1,'Set_Distribution_Type(const DCT_Distribution, const DCT_Integer *, DCT_Distribution *, DCT_Integer **):&#160;dct_data_utils.c'],['../da/df2/dct__data__utils_8c.html#a9bc6d8daaaed625ae3131725134eeb34',1,'Set_Distribution_Type(const DCT_Distribution dist, const DCT_Integer *procs, DCT_Distribution *vartype, DCT_Integer **varprocs):&#160;dct_data_utils.c']]],
  ['sndrnpts',['SndrNpts',['../db/d1a/structDCT__SDConsume__Plan.html#af17e26d295620194655a628158808a09',1,'DCT_SDConsume_Plan']]],
  ['sndrnsd',['SndrNSD',['../db/d1a/structDCT__SDConsume__Plan.html#acd8fa03d02eb05fde26665facaddc4f4',1,'DCT_SDConsume_Plan']]],
  ['sndrsdom',['SndrSDom',['../db/d1a/structDCT__SDConsume__Plan.html#a7bef5b55cdd6aa78e480c8e38f70504f',1,'DCT_SDConsume_Plan']]]
];
